import { container, title } from "assets/jss/material-kit-react.js";
import headerLinksStyle from "assets/jss/material-kit-react/components/headerLinksStyle.js";

const navbarsStyle = theme => ({
  section: {
    padding: "70px 0",
    paddingTop: "0"
  },
  container,
  title: {
    ...title,
    marginTop: "30px",
    minHeight: "32px",
    textDecoration: "none"
  },
  navbar: {
    marginBottom: "-20px",
    zIndex: "100",
    position: "relative",
    overflow: "hidden",
    "& header": {
      borderRadius: "0"
    }
  },
  navigation: {
    backgroundPosition: "center center",
    backgroundSize: "cover",
    marginTop: "0",
    minHeight: "740px"
  },
  formControl: {
    margin: "0 !important",
    paddingTop: "0"
  },
  inputRootCustomClasses: {
    margin: "0!important"
  },
  searchIcon: {
    width: "20px",
    height: "20px",
    color: "inherit"
  },
  ...headerLinksStyle(theme),
  logoImg: {
    width: "248px",
    height: "74px",
   
  },
  img: {
    width: "60px",
    height: "60px",
    borderRadius: "50%",
   
  },
  imageDropdownButton: {
    padding: "0px",
    top: "4px",
    borderRadius: "50%",
    marginLeft: "5px"
  },
  homeIcon: {
    "@media (min-width: 657px)": {
      display: 'none'
    },
  },
  fileTab: {
    backgroundColor: '#1f3a6e',
    display: 'grid',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    height: '400px',
    width: 'fit-content'

  },
  paragraph: {
    color: '#fffff'
  },

  personalInfoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: '1rem',
  },
  gutters: {
    paddingLeft: '16px',
    paddingRight: '0',

  }
});

export default navbarsStyle;
