import { container } from "assets/jss/material-kit-react.js";
import greenBG from 'assets/img/svgBG/greenBG.png'
import redBG from 'assets/img/svgBG/redBG.png'
import yellowBG from 'assets/img/svgBG/yellowBG.png'
import userBg from "../../../../assets/img/svgBG/userIcon.png"
import userBgIcon from "assets/img/svgBG/Ellipse.svg"
const componentsStyle = {
  container,
  brand: {
    color: "#FFFFFF",
    textAlign: "left"
  },
  title: {
    fontSize: "4.2rem",
    fontWeight: "600",
    display: "inline-block",
    position: "relative"
  },
  subtitle: {
    fontSize: "1.313rem",
    maxWidth: "500px",
    margin: "10px 0 0"
  },
  main: {
    background: "#FFFFFF",
    position: "relative",
    zIndex: "0"
  },
  mainRaised: {
    
    borderRadius: "6px",
    boxShadow:
      "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",
    borderRadius: '6% 0% 0% 0%',
    backgroundColor: '#1f3a6e'
  },
  mainUNRaised:{
   
    borderRadius: "6px",
    boxShadow:
      "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",
    backgroundColor: '#1f3a6e'
  },
  link: {
    textDecoration: "none"
  },
  textCenter: {
    textAlign: "center"
  },
  fixed: {
    position: "fixed",
    zIndex: "1400"
  },
  fixedOpen: {
    position: "fixed",
    zIndex: "1400",
    width: '81.5%'
  },

  menuOpened: {
    width: '82.5%',
    right: '0',
    alignItems: 'end',
    display: 'inline-table',
    paddingRight: '0',
    marginRight: '-17px'
  },

  menuClosed: {
    marginLeft: '0%'
  },
  menuBtn: {
    padding: '0px',
    fontWeight: 'bold',
    fontSize: 'medium'
  },
  logoImg: {
    width: '40vw',
    height: '7vh',
    marginRight: '30vw'

  },
  bgNotifBtn:{
    backgroundColor: 'transparent',
    border: '0',
    boxShadow: "none",
    "&,&:hover,&:focus": {
      backgroundColor: "transparent"
    }
  },
  fileTab: {
    backgroundColor: '#1f3a6e',
    display: 'block',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    height: '6vh',
    width: 'fit-content',
    padding: '1vh'

  },
  bgTabBtn:{
    backgroundColor: "transparent",
    width: '15%',
    boxShadow: "none",
  
  },
  genTabContainer:{
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '4vh',
    background: "-webkit-gradient(linear, left top, right top, from(#254176), color-stop(10%, #254176), color-stop(96%, #1c3668))",
  },

  styleContainerIconGreen: {
    width: '15vw', height: '8vh', borderRadius: '50%', backgroundImage: `url(${greenBG})`,
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundSize: '100% 100%',
    backgroundRepeat: 'no-repeat',
    marginTop: '2vh'
  },
  styleContainerIconYellow: {
    width: '15vw', height: '8vh', borderRadius: '50%', backgroundImage: `url(${yellowBG})`,
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundSize: '100% 100%',
    backgroundRepeat: 'no-repeat',
    marginTop: '2vh'
  },
  styleContainerIconRed: {
    width: '15vw', height: '8vh', borderRadius: '50%', backgroundImage: `url(${redBG})`,
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundSize: '100% 100%',
    backgroundRepeat: 'no-repeat',
    marginTop: '2vh'
  },
  genTabLabel:{
    
    fontWeight: 'Cairo'
  },
  genLabel: {
    marginBottom: '-1vw',
    color: '#ffffff',
  },
  genLabelSpeed: {
    color: '#a7b5d2',
  },

  userName: {
    textAlign: 'end',
    fontFamily: 'Cairo',
    fontWeight: '700',
    display: 'block',
    fontSize: '16px',
  },
  usersContainer: {
    width: '100%',
    marginRight: '0',
    textAlign: 'right',
    marginTop: '3vh'
  },
  privacyContainer: {
    textAlign: 'center',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'

  },
  bgIcon: {
    color: '#e83866',
    backgroundImage: `url(${userBg})`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: '3vw',
    height: '8vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  itemContainer: {
    "&:hover,&:focus": {
      borderRadius: '0% 35% 30% 0%'
    },
    height: '8vh',
    marginBottom: '4%',
    borderRadius: '0% 35% 30% 0%'
  },
  devider: {
    width: '10%',
    fontWeight: 'bold',
    height: '0.3vh',
    marginLeft: '60%',
  },

  time: {
    fontSize: '11px',
    lineHeight: '0.5em'
  },
  privacyChild: {
    width: '65%',
    flexWrap: 'wrap',
    marginTop: '9%',
    textAlign: 'end',
    marginLeft: '20%'
  },
  socialLine: {
    color: '#889dc7'
  },
};

export default componentsStyle;
