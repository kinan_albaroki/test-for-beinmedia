import {
  defaultFont,

  primaryColor,
  lightColor,
  infoColor,
  successColor,
  warningColor,
  dangerColor,
  claudeBlueColor
} from "assets/jss/material-kit-react.js";

const typographyStyle = {
  defaultFontStyle: {
    ...defaultFont,
   
    fontSize: "14px"
  },
  defaultHeaderMargins: {
    marginTop: "20px",
    marginBottom: "10px"
  },
  quote: {
    padding: "10px 20px",
    margin: "0 0 20px",
    fontSize: "17.5px",
    
    color: "#ffffff"
  },
  quoteText: {
    margin: "0 0 10px",
    color: "#fffafab5",
    overflow: 'auto',
    padding: '10px'
  },
  quoteAuthor: {
    display: "block",
    fontSize: "80%",
    lineHeight: "1.42857143",
    color: "#fffff"
  },
  quoteContainer: {
    overflow: 'auto',
    padding: '10px',
    boxShadow: '- 32px 60px 33px 0 rgba(29, 66, 139, 0.51)',
    backgroundColor: '#2a4986',
    borderRadius: '5px'
  },
  titleTag: {
    fontWeight: '800',
    fontSize: '21px',
    lineHeight: '1.8em',
    color: '#ffffff'
  },
  mutedText: {
    color: "#777"
  },
  paragraph: {
    color: "#ffffff"
  },

  primaryText: {
    color: lightColor
   },
   grayText: {
     color: claudeBlueColor,
     paddingRight: '5px',
   },
  infoText: {
    color: infoColor
  },
  successText: {
    color: successColor
  },
  warningText: {
    color: warningColor
  },
  dangerText: {
    color: dangerColor
  },
  smallText: {
    fontSize: "65%",
    fontWeight: "400",
    lineHeight: "1",
    color: "#a09f9f"
  },

  cardContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    paddingRight: '16px',
    paddingLeft: '16px',
    justifyContent: 'center'
  },
  showViewCourse: {
    display: 'block'
  },
  hideViewCourse: {
    display: 'none'
  },
  hideCourses: {
    display: 'none'
  },

  container: {
    paddingRight: '16px',
    paddingLeft: '16px',
  },
  testIconContainer: {
    height: '40vh',
    width: '100%',
    display: 'flex',
    alignItem: 'center',
    justifyContent: 'center'
  }
};

export default typographyStyle;
