import { defaultFont } from "assets/jss/material-kit-react.js";
import tooltip from "assets/jss/material-kit-react/tooltipsStyle.js";


import userBg from "../../../../assets/img/svgBG/userIcon.png"
import userBgIcon from "assets/img/svgBG/Ellipse.svg"

const headerLinksStyle = theme => ({
  list: {
    ...defaultFont,

    fontSize: "14px",
    margin: 0,
    paddingLeft: "0",
    listStyle: "none",
    paddingTop: "0",
    paddingBottom: "0",
    color: "#1f3a6e",
    justifyContent: 'center',
    alignItem: 'center',
    alignSelf: 'center',
    display: 'flex'

  },
  listMenu: {
    ...defaultFont,
 
    fontSize: "14px",
    margin: 0,
    paddingLeft: "0",
    listStyle: "none",
    paddingTop: "0",
    paddingBottom: "0",
    color: "#1f3a6e",
    justifyContent: 'center',
    alignItem: 'center',
    alignSelf: 'center',
    display: 'block',
  },
  listItem: {
    float: 'right',
    display: "inline-flex",
    color: "#1f3a6e",
    position: "relative",
    width: "auto",
    margin: "0",
    paddingBottom: '0',
    paddingLeft: '4px',
    paddingRight: '0',
    paddingTop: '0',
    justifyContent: 'center',
    alignItem: 'center',
    alignSelf: 'center',
  
    
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      "&:after": {
        width: "calc(100% - 30px)",
        content: '""',
        display: "block",
        height: "1px",
        marginLeft: "15px",
        backgroundColor: "#e5e5e50000"
      }
    }
  },
  listItemText: {
    padding: "0 !important"
  },
  navLink: {
    color: "#1f3a6e",
    position: "relative",
    padding: "0.9375rem",
    fontWeight: "400",
    fontSize: "12px",
    textTransform: "uppercase",
    borderRadius: "3px",
    lineHeight: "20px",
    textDecoration: "none",
    margin: "0px",
    display: "inline-flex",
    "&:hover,&:focus": {
      color: "#1f3a6e",
      background: "rgba(200, 200, 200, 0.2)"
    },
    [theme.breakpoints.down("sm")]: {
      width: "calc(100% - 30px)",
      marginLeft: "15px",
      marginBottom: "8px",
      marginTop: "8px",
      textAlign: "left",
      "& > span:first-child": {
        justifyContent: "flex-start"
      }
    }
  },
  notificationNavLink: {
    color: "#1f3a6e",
    padding: "0.9375rem",
    fontWeight: "400",
    fontSize: "12px",
    textTransform: "uppercase",
    lineHeight: "20px",
    textDecoration: "none",
    margin: "0px",
    display: "inline-flex",
    top: "4px"
  },
  registerNavLink: {
    top: "3px",
    position: "relative",
    fontWeight: "400",
    fontSize: "12px",
    textTransform: "uppercase",
    lineHeight: "20px",
    textDecoration: "none",
    margin: "0px",
    display: "inline-flex"
  },
  navLinkActive: {
    color: "#1f3a6e",
    backgroundColor: "rgba(255, 255, 255, 0.1)"
  },
  icons: {
   fontSize: "20px"
  },
  socialIcons: {
    position: "relative",
    fontSize: "20px !important",
    marginRight: "4px"
  },
  dropdownLink: {

    "&,&:hover,&:focus": {
      color: "inherit",
      textDecoration: "none",
      display: "block",
      padding: "10px 20px"
    },
    color: "#1f3a6e",
    textAlign: 'end'
  },
  ...tooltip,
  marginRight5: {
    marginRight: "5px"
  },
  welcomText: {
    width: '6vw',
    height: '3vh',
    fontFamily: 'Cairo',
    fontSize: '18px',
    fontWeight: 'normal',
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: '1.81',
    letterSpacing: 'normal',
    textAlign: 'right',
    paddingRight: '5px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },

  userText: {
    width: '6vw',
    height: '3vh',
    ...defaultFont,
 
    fontSize: '18px',
    fontWeight: 'bold',
    fontStretch: 'normal',
    fontStyle: 'normal',
    lineHeight: '1.81',
    letterSpacing: 'normal',
    textAlign: 'right',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  
   
  },
  userIcon:{
    color: "linear - gradient(43deg, #e83866 17 %, #e57a3c 69 %)"
  },

  usesrBtn: {
    backgroundImage: `url('assets/img/svgBG/Ellipse.svg')`,
    backgroundColor: '#e9edf0',
    border: '1px',
    borderColor: '#a7b5d2'
  },
  Ellipse1: {
    width: '45px',
    height: '45px',
    objectFit: 'contain',
    boxShadow: '0 0 5px 0 rgba(0, 0, 0, 0.16)',
    border: 'solid 2px var(--cloudy - blue)',
    
},


  menuBtn: {
    padding: '0px',
    fontWeight: 'bold',
    fontSize: 'medium'
  },
   Badge: {
     backgroundColor: 'tranparent', 
     color: '#e9edf0',
     fontWeight: '900',
  
   },

  menuContainer: {
    fontWeight: '800',
    width: '100%',
  },
  menuStyle: {
    width: '15%',
    position: 'sticky',
    zIndex: '1',
    right: '0px',
    bottom: '0px',
    top: '0',
    left: '0px',
    
  },
  userName: {
    textAlign: 'end',
    fontFamily: 'Cairo',
    fontWeight: '700',
    display: 'block',
    fontSize: '16px',
  },

  usersContainer:{
    width: '100%',
    marginRight: '0',
    textAlign: 'right',
    marginTop: '3vh'
  },

  privacyContainer: {
    textAlign: 'center',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'

  },

  bgIcon: {
    color: '#e83866',
    backgroundImage: `url(${userBg})`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: '3vw',
    height: '7vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },

  itemContainer: {
    "&:hover,&:focus": {
      borderRadius: '0% 35% 30% 0%'

    },

    height: '8vh',
    marginBottom: '5%',
    borderRadius: '0% 35% 30% 0%'
  },
  devider: {
    width: '10%',
    fontWeight: 'bold',
    height: '0.3vh',
    marginLeft: '60%',
  },

  time:{
    fontSize: '11px',
    lineHeight: '0.5em'
  },
  privacyChild: {
    width: '65%',
    flexWrap: 'wrap',
    marginTop: '9%',
    textAlign: 'end',
    marginLeft: '20%'
  },
  socialLine:{
    color: '#889dc7'
  },

  bgUserBtn: {
    backgroundImage: `url(${userBgIcon})`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
  }

});

export default headerLinksStyle;
