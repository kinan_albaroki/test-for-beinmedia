
import imagesStyle from "assets/jss/material-kit-react/imagesStyles.js";

const cardStyle = {
  card: {
    border: "0", 
    borderRadius: "6px",
    color: "rgba(0, 0, 0, 0.87)",
    background: "#fff",
    width: "100%",
    boxShadow:
      "0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12)",
    position: "relative",
    display: "flex",
    flexDirection: "column",
    minWidth: "0",
    wordWrap: "break-word",
    fontSize: ".875rem",
    transition: "all 300ms linear"
  },
  ...imagesStyle,
  cardPlain: {
    background: "transparent",
    boxShadow: "none",
    minHeight: '200px',
  },
  cardCustom: {
    background: "transparent",
    boxShadow: "none",
    minHeight: '200px',
    height: '260px'
  },
  cardCarousel: {
    overflow: "hidden"
  },
  container: {
    width: "30%",
    height: '250px',
    paddingLeft: '5px',
    paddingRight: '5px',
    "@media (max-width: 640px)": {
      width: "45%",
    },
  },

  CoursesContainer: {
    width: "25%",
    height: '300px',
    paddingLeft: '30px',
    paddingRight: '30px',
    "@media (max-width: 640px)": {
      width: "100%",
      height: '44vh',
      paddingLeft: '2vw',
      paddingRight: '2vw',
    },
  },

  videoPlayer: {
      borderRadius: '12px',
      marginBottom: '2vw',  
    
  },
  videoPlayerMobile: {
    display: 'none'
  },
  cardTitle: {
    color: '#ffffff',
    fontSize: '14px',
    lineHeight: '1.43'
  },
  smallTitle: {
    color: '#8191b2',
    fontSize: '13px',
    lineHeight: '1.53',
   
  
  },
  customSmallTitle: {
    color: '#8191b2',
    fontSize: '13px',
    lineHeight: '1.53',
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  icon: {
    color: '#e57a3c'
  }
};

export default cardStyle;
