
import React from "react";
import { Link } from "react-router-dom";


import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import NotificationsActiveOutlinedIcon from '@material-ui/icons/NotificationsActiveOutlined';
import CustomDropdown from "components/CustomDropdown/CustomDropdown.js";
import Button from "components/CustomButtons/Button.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import styles from "assets/jss/material-kit-react/components/headerLinksStyle.js";


const useStyles = makeStyles(styles);

export default function HeaderLinks(props) {
  const classes = useStyles();
  return (
    <List className={classes.list}>
      
      <ListItem className={classes.listItem}>
        <GridContainer justify="center">
          <GridItem xs={12} sm={12} md={8}>
            <CustomDropdown
              noLiPadding
              buttonProps={{
                className: classes.navLink,
                color: "transparent"
              }}
              buttonIcon={NotificationsActiveOutlinedIcon}
              dropdownList={[
                <Link className={classes.dropdownLink}>
                  الإشعار رقم 1
            </Link>,

                <Link className={classes.dropdownLink}>
                  الإشعار رقم 2
            </Link>,

                <Link className={classes.dropdownLink}>
                  الإشعار رقم3
            </Link>
              ]}
            />
          </GridItem>
        </GridContainer>
      </ListItem>

      <ListItem className={classes.listItem}>
        <Link to={"/login-page"}>
          <Button
            justIcon
            round
            color='#e9edf0'
            className={classes.bgUserBtn}
          >
            <i class="far fa-user-circle fa-2x" style={{ color: '#e83866' }}></i>
          </Button>
        </Link>

        <h6 className={classes.welcomText}>مرحبا بك</h6> <h5 className={classes.userText}>مالك محمد</h5>  
     
      </ListItem>

      
    </List>
  );
}
