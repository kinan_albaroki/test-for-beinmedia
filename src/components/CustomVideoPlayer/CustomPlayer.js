import React from "react";

import classNames from "classnames";

import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/material-kit-react/components/cardStyle.js";

import ReactPlayer from 'react-player'

const useStyles = makeStyles(styles);


export default function CustomPlayer(props) {
    const classes = useStyles();
    const { className, children, plain, carousel, ...rest } = props;
    const cardClasses = classNames({
        [classes.card]: true,
        [classes.cardPlain]: plain,
        [classes.cardCarousel]: carousel,
        [className]: className !== undefined
    });

    console.log('prpppp', props.isMobile);
    return (
        
        <ReactPlayer
            className={classes.videoPlayer}
         controls="false"
            width="100%"
            height="70vh"
            url={props.url}
            config={{
                youtube: {
                    playerVars: { showinfo: 0 }
                }
            }}
        />


       
    );
}

CustomPlayer.propTypes = {
    className: PropTypes.string,
    plain: PropTypes.bool,
    carousel: PropTypes.bool,
    children: PropTypes.node
};
