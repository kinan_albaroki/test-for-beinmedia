import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import styles from "assets/jss/material-kit-react/components/cardStyle.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";

const useStyles = makeStyles(styles);




export default function CustomCardList(props ) {
    const classes = useStyles();
    const { className, children, plain, carousel,id , ...rest } = props;
    const cardClasses = classNames({
        [classes.card]: true,
        [classes.cardPlain]: plain,
        [classes.cardCarousel]: carousel,
      
        [className]: className !== undefined
    });

    const imageClasses = classNames(
        classes.imgRaised,
        classes.imgCard,
        classes.imgFluid
    );



    return (

    
        <div className={props.containerStyle === 'CoursesContainer' ? classes.CoursesContainer : classes.container} onClick={props.onClick} >
                    <Card custom>
                        <GridItem xs={12} sm={12} md={12} className={classes.itemGrid}>
                            <img src={props.imgSrc} alt="..." className={imageClasses} />
                        </GridItem>
                        <h4 className={classes.cardTitle}>
                                {props.courseText}
                            <br />
                    <small className={props.containerStyle === 'CoursesContainer' ? classes.customSmallTitle : classes.smallTitle}> {props.courseSalary + '  ' + '  '} <i class="fas fa-download">  12 </i> </small>
                       
                        </h4>
                         
                
                    </Card>
            </div>
     
    );
}

CustomCardList.propTypes = {
    className: PropTypes.string,
    plain: PropTypes.bool,
    carousel: PropTypes.bool,
    children: PropTypes.node
};
