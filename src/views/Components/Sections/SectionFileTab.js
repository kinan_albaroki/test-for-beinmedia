import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Header from "components/Header/Header.js";
import styles from "assets/jss/material-kit-react/views/componentsSections/navbarsStyle.js";
import Primary from "components/Typography/Primary.js";

import logoImage from "assets/img/logo/logo.png";
import profileImg from "assets/img/profile.png";
import SecondPrimary from "components/Typography/SecondPrimary";
import Badge from "components/Badge/Badge.js";

const useStyles = makeStyles(styles);

export default function SectionFileTab() {
    const classes = useStyles();
    return (
        <Header
            color="primary"
            className={classes.fileTab}
            container
            rightLinks={
            <img
                src={logoImage}
                className={classes.logoImg}
                alt="logo"
            />}
            leftLinks={
                <List className={classes.list}>
                

                    <ListItem className={classes.listItem}>
                        <i class="fas fa-home fa-s" className={classes.homeIcon} style={{ color: '#435f95' }}></i>
                    </ListItem>

                    <ListItem className={classes.listItem}>
                        <img
                            src={profileImg}
                            className={classes.img}
                            alt="profile"
                        />
                    </ListItem>

                    <GridContainer className={classes.listItem}>
                     
                        <GridItem className={classes.personalInfoContainer}>
                            <Primary>العيادة الرقمية</Primary>
                            <SecondPrimary>ل د.هند الناهض</SecondPrimary>
                            <ListItem className={classes.gutters} ><i class="fas fa-star fa-xs" style={{ color: '#435f95' }}></i><i class="fas fa-star fa-xs" style={{ color: '#E75C50' }}></i><i class="fas fa-star fa-xs" style={{ color: '#E75C50' }}></i><i class="fas fa-star fa-xs" style={{ color: '#E75C50' }}></i><SecondPrimary >تقييم</SecondPrimary></ListItem>

                        </GridItem>
                      
                    </GridContainer>

                    
                   
                   
                    <ListItem className={classes.listItem}>
                        <Badge color="gradiant" className={classes.Badge}>مشغول باستشارة</Badge>
                    </ListItem>
                </List>
            }
        />

    )
}