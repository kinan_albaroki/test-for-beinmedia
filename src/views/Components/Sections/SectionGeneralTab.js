import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";

import styles from "assets/jss/material-kit-react/views/componentsSections/tabsStyle.js";
import AboutPage from '../AboutPage/AboutPage';
import StorePage from '../StorePage/StorePage';
import BroadcastPage from "../BroadcastPage/BroadcastPage";
import CoursesPage from "../CoursesPage/CoursesPage";
import TestAudio from "../OtherPages/TestAudio";
import TestVideo from "../OtherPages/TestVideo";
import TestInternet from "../OtherPages/TestInternet";

import greenBG from 'assets/img/svgBG/greenBG.png'
import redBG from 'assets/img/svgBG/redBG.png'
import yellowBG from 'assets/img/svgBG/yellowBG.png'
const useStyles = makeStyles(styles);

export default function SectionGeneralTab(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const [activatedTab, setTab] = React.useState(0);
    const isMobile = props.isMobile;
    const isWeb = props.isWeb
    const handleChange = (event, value) => {
        setValue(value);
    };
    return (
        <GridContainer dir="rtl" >
            <GridItem xs={12} sm={12} md={12} classNames={classes.cardContainer}>
                            <CustomTabs
                                plainTabs
                                headerColor="primary"
                                handleChange={handleChange}
                                active = {0}
                                isOpen={props.isOpen}
                                tabs={[
                                    {
                                        tabName: "نبذة عن هند",
                                        tabContent: (
                                            <AboutPage isMobile={isMobile} isWeb={isWeb} />
                                        )
                                    },
                                    {
                                        tabName: "متجر",
                                        
                                        tabContent: (
                                            <StorePage isMobile={isMobile} isWeb={isWeb} />
                                        )
                                    },
                                    {
                                        tabName: "برودكاست",
                                        
                                        tabContent: (
                                            <BroadcastPage isMobile={isMobile} isWeb={isWeb} />
                                        )
                                    },
                                    {
                                        tabName: "كورسات",
                                        tabContent: (
                                            <CoursesPage changeActiveTab={activatedTab => {
                                                setTab(activatedTab)}} 
                                                isMobile={isMobile} isWeb={isWeb} />
                                        ),
                                    },
                                    {
                                        tabName: "حجز عيادة (20 دينار)كويتي",
                                        tabContent: (
                                            <p className={classes.textCenter}>
                                            </p>
                                        )
                                    },
                                    {},
                                    {},
                                    {
                                        tabName: "تجربة الصوت",
                                        tabIcon: 'fas fa-microphone',
                                        styleIcon: {paddingRight: '20%', color: '#1dd950', padding: '15px', borderRadius: '50%', border: '1px #1dd950', margin: '0px 5px 0px 5px',

                                    },
                                        styleContainerIcon: {
                                            width: '4vw', height: '8vh', borderRadius: '50%', backgroundImage: `url(${greenBG})`,   
                                            textAlign: 'center',
                                            display: 'flex',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            backgroundSize: '100% 100%',
                                            backgroundRepeat: 'no-repeat',
                                            marginTop: '5%'
                                    },
                                        tabContent: (
                                           <TestAudio />
                                        )
                                    },

                                    {
                                        tabName: "تجربة الكاميرا",
                                        tabIcon: "fas fa-video",
                                        styleIcon: { color: '#e21956', padding: '15px', borderRadius: '50%', border: '1px #e21956', margin: '0px 5px 0px 5px' },
                                        styleContainerIcon: {
                                             width: '4vw', height: '8vh', borderRadius: '50%', backgroundImage: `url(${redBG})`,   
                                            textAlign: 'center',
                                            display: 'flex',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            backgroundSize: '100% 100%',
                                            backgroundRepeat: 'no-repeat',
                                            marginTop: '5%'
                                    },
                                       
                                        tabContent: (
                                            <TestVideo />
                                        )
                                    },
                                    {
                                        tabName: "سرعة الأنترنت",
                                        tabIcon: "fas fa-wifi",
                                        styleIcon: { color: '#e28519', padding: '15px', borderRadius: '50%', border: '1px #e28519', margin: '0px 5px 0px 5px' },
                                        styleContainerIcon: {
                                            width: '4vw', height: '8vh', borderRadius: '50%', backgroundImage: `url(${yellowBG})`,
                                            textAlign: 'center',
                                            display: 'flex',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            backgroundSize: '100% 100%',
                                            backgroundRepeat: 'no-repeat',
                                            marginTop: '5%'
                                        },
                                        
                                        tabContent: (
                                            <TestInternet />
                                        )
                                    }
                                ]}
                            />
                        </GridItem>
                    </GridContainer>
          
    )
}