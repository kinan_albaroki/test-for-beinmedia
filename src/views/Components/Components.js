import React from "react";

import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import Header from "components/Header/Header.js";

// sections for this page
import HeaderLinks from "components/Header/HeaderLinks.js";
import MenuLinks from "../../components/Menu/MenuLinks";

import SectionFileTab from "./Sections/SectionFileTab.js";

import SectionGeneralTab from "./Sections/SectionGeneralTab.js";
import Badge from "components/Badge/Badge.js";
import Button from "components/CustomButtons/Button.js";
import styles from "assets/jss/material-kit-react/views/components.js";
import Divider from '@material-ui/core/Divider';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import userImage from "assets/icons/user.png";
import userImageActive from "assets/icons/userActive.png";

import ListItemIcon from '@material-ui/core/ListItemIcon';

import logoImage from "assets/img/logo/logo.png";

import {
  useWindowWidth,
} from '@react-hook/window-size'


const useStyles = makeStyles(styles);

export default function Components(props) {
  const classes = useStyles();
  const { ...rest } = props;
  const [opened, setOpened] = React.useState(false)
  const isOpen = (data) => {
      setOpened(!opened);
  }
  const [user, setUser] = React.useState([
    { userName: 'مستخدم 1', watingTime: 'منذ 15 دقيقة', icon: 'not-active' },
    { userName: 'مستخدم 2', watingTime: 'منذ 13 دقيقة', icon: 'not-active' },
    { userName: 'مستخدم 3', watingTime: 'منذ 9 دقيقة', icon: 'not-active' },
    { userName: 'مالك محمد', watingTime: 'منذ 7 دقيقة', icon: 'active' },
    { userName: 'مستخدم 4', watingTime: 'منذ 20 دقيقة', icon: 'not-active' },
    { userName: 'مستخدم 5', watingTime: 'منذ 10 دقيقة', icon: 'not-active' },
    { userName: 'مستخدم 6', watingTime: 'منذ 2 دقيقة', icon: 'not-active' },

  ]);
  const screenSize = useWindowWidth();

  const [mobileState, setMobileState] = React.useState(true);

  const [webState, setWebState] = React.useState(true);

  const toggleDir = ( ) => {
    if (screenSize < 640) {
      setMobileState(true);
      setWebState(false);
    } else {
      setMobileState(false);
      setWebState(true);
    }
  }

  const MobileTab = () => {
    return (
      <>
      <div style={{display: 'flex', textAlign: 'center', justifyContent: 'center'}}>
      <Badge color="transperansy" className={classes.Badge}>14</Badge>
      <Button color="primary" transparent simple className={classes.menuBtn}>قائمة الإنتظار</Button>
        </div>
      </>
    )
  }


  const MobileFileTab = () => {
    return (
      <>
        <div
          color="primary"
          className={classes.fileTab}
          container
        >
          <>
            <Button
              justIcon
              className={classes.bgNotifBtn}
            >
              <i class="far fa-bell fa-2x" style={{ color: '#ffffff' }}></i>
            </Button>

            <Link to={"/login-page"}>
              <Button
                justIcon
                round
                color='#e9edf0'
                className={classes.bgUserBtn}
              >
                <i class="far fa-user-circle fa-2x" style={{ color: '#e83866' }}></i>
              </Button>
            </Link>
          </>

          <img
            src={logoImage}
            className={classes.logoImg}
            alt="logo"
          />


        </div>
      </>
    )
  }


  const MobileGeneralTab = () => {
    return (
      <>
        <div className={classes.genTabContainer}>
          <Button
            endIcon={<div className={classes.styleContainerIconGreen}><i class="fas fa-microphone fa-2x" style={{
              paddingRight: '20%', color: '#1dd950', padding: '15px', borderRadius: '50%', border: '1px #1dd950', margin: '0px 5px 0px 5px',
            }}></i></div>}
            color='#ffffff'
            className={classes.bgTabBtn}
          >

          </Button>

          <Button
            endIcon={<div className={classes.styleContainerIconRed}><i class="fas fa-video fa-2x" style={{ 
              color: '#e21956', padding: '15px', borderRadius: '50%', border: '1px #e21956', margin: '0px 5px 0px 5px'
            }}></i></div>}
            color='#ffffff'
            className={classes.bgTabBtn}
          >
             
          </Button>

        <Button
            endIcon={<div className={classes.styleContainerIconYellow}><i class="fas fa-wifi fa-2x" style={{
            color: '#e28519', padding: '15px', borderRadius: '50%', border: '1px #e28519', margin: '0px 5px 0px 5px'
          }}></i></div>}
          color='#fffff'
          className={classes.bgTabBtn}
        >
          </Button>
          <div className={classes.genTabLabel}>
            <h6 className={classes.genLabel}>سرعة الأنترنت</h6>
            <p className={classes.genLabelSpeed} dir="ltr">36 Mbps</p>
          </div>
        </div>
        
      </>
    )
  }


  const MobileMenuTab = () => {
    return (
      <>
        <List className={classes.usersContainer}>
          {user.map((user) => (
            <ListItem button key={user.userName} className={classes.itemContainer}>
              <ListItem className={classes.userName}  ><h6 className={classes.time}>{user.watingTime}</h6>{user.userName}</ListItem>
              <ListItemIcon className={classes.bgIcon}>{user.icon === 'active' ? <div style={{}}>
                <img src={userImageActive}>
                </img></div>
                : <div><img src={userImage}>
                </img></div>}</ListItemIcon>

            </ListItem>
          ))}
        </List>
        <Divider />
        <div className={classes.privacyContainer}>
          <div className={classes.privacyChild}>
            <p style={{ color: '#889dc7' }}> جميع الحقوق محفوظة تايم فيوير @ 2013-2020</p>
            <div className={classes.socialLine}>
              <Button
                justIcon
                href="#pablo"
                target="_blank"
                color="transparent"
                onClick={e => e.preventDefault()}
                style={{ color: '#889dc7' }}
              >
                <i className={classes.socialIcons + " fab fa-twitter"} />
              </Button>
              <Button
                justIcon
                href="#pablo"
                target="_blank"
                color="transparent"
                onClick={e => e.preventDefault()}
                style={{ color: '#889dc7' }}
              >
                <i className={classes.socialIcons + " fab fa-facebook"} />
              </Button>
              <Button
                justIcon
                href="#pablo"
                target="_blank"
                color="transparent"
                onClick={e => e.preventDefault()}
                style={{ color: '#889dc7' }}
              >
                <i
                  className={
                    classes.socialIcons + " fab fa-instagram"
                  }
                />
              </Button>
            </div>
          </div>
        </div>
      </>
    )
  }
  
  return (
    <div className={opened ? classes.menuOpened : classes.menuClosed} onLoad={toggleDir} dir='rtl'>
      <Header
        rightLinks={mobileState === false ? <MenuLinks open={(open) => isOpen(open)} /> : <MobileMenuTab />   }
        leftLinks={mobileState === false ? <HeaderLinks /> : <MobileTab />}
        className={opened === true ? classes.fixedOpen : classes.fixed}
        color="white"
        changeColorOnScroll={{
          height: 400,
          color: "#1f3a6e"
        }}
        
      />


      <div className={classNames(classes.main, opened ? classes.mainRaised : classes.mainUNRaised )}>
        {mobileState === true ? <MobileFileTab /> : ''}
        {mobileState === true ? <MobileGeneralTab /> : ''}
        <SectionFileTab />
        <SectionGeneralTab isMobile={mobileState} isWeb={webState} isOpen={opened}/>
      </div>
    </div>
  );
}
