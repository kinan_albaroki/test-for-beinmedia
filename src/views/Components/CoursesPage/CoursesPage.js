import React from "react";

import CustomCardList from '../../../components/CustomCardList/CusromCardList';
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import { makeStyles } from "@material-ui/core/styles";

import styles from "assets/jss/material-kit-react/components/typographyStyle.js";
import team1 from "../../../assets/img/faces/faceCard.png";
import team2 from "../../../assets/img/svgBG/videoBG22.png";
import team3 from "../../../assets/img/svgBG/videoBG33.png";
import team4 from "../../../assets/img/svgBG/videoBG44.png";

import { useHistory } from "react-router-dom";
import ViewCoursePage from "../ViewCoursePage/ViewCoursePage";

const useStyles = makeStyles(styles);

export default function CoursesPage(props) {
    const classes = useStyles();
    const history = useHistory();
    const [course, setCourse] = React.useState([
        { courseText: 'تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '1', imgSrc: team1, courseSalary: '299 دينار كويتي' },
        { courseText: 'تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '2', imgSrc: team2, courseSalary: '340 دينار كويتي' },
        { courseText: 'دورة صناعة المحتوى مع هند الناهض', id: '3', imgSrc: team3, courseSalary: '500 دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '4', imgSrc: team4, courseSalary: '190 دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '5', imgSrc: team1, courseSalary: '299 دينار كويتي' },
        { courseText: ' تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '6', imgSrc: team2, courseSalary: '299 دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '4', imgSrc: team4, courseSalary: '190 دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '5', imgSrc: team3, courseSalary: '299 دينار كويتي' },
        { courseText: ' تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '6', imgSrc: team2, courseSalary: '299 دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '4', imgSrc: team1, courseSalary: '190 دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '5', imgSrc: team4, courseSalary: '299 دينار كويتي' },
        { courseText: ' تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '6', imgSrc: team3, courseSalary: '299 دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '4', imgSrc: team2, courseSalary: '190 دينار كويتي' },
        { courseText: 'فوكس حلقة ٥', id: '5', imgSrc: team1, courseSalary: '299 دينار كويتي' },
        { courseText: ' تعرف على أسرار صناعة المحتوى التسويقي الفعال', id: '6', imgSrc: team4, courseSalary: '299 دينار كويتي' },
        { courseText: ' دورة صناعة المحتوى مع هند الناهض', id: '4', imgSrc: team4, courseSalary: '190 دينار كويتي' },
    ])

    
    const [showViewCourse, setShowViewCourse] = React.useState(false);


   const  changeActiveTab = (course ) => {
      
    setShowViewCourse(true)
   }
    return (
        <GridContainer justify="space-between"  >
            <GridItem xs={12} sm={12} md={12} className={showViewCourse === false ? classes.cardContainer   : classes.hideCourses }>
                {course.map((course) => (
                   
                    <CustomCardList containerStyle={'CoursesContainer'} imgSrc={course.imgSrc} courseText={course.courseText} courseSalary={course.courseSalary} onClick={() => changeActiveTab(course)}/>
                ))}
            </GridItem>

            <div className={showViewCourse === true ? classes.showViewCourse : classes.hideViewCourse}>
                <ViewCoursePage isMobile={props.isMobile} isWeb={props.isWeb} />
            </div>
        </GridContainer>

        
    )
}