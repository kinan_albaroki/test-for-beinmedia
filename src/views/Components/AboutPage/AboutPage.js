import React from "react";
import CustomPlayer from '../../../components/CustomVideoPlayer/CustomPlayer';
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Quote from "components/Typography/Quote.js";
import { makeStyles } from "@material-ui/core/styles";
import Button from "components/CustomButtons/Button.js";
import styles from "assets/jss/material-kit-react/components/typographyStyle.js";
import CustomMobilePlayer from "components/CustomVideoPlayer/CustomMobilePlayer";
const useStyles = makeStyles(styles);

export default function AboutPage(props) {
    const classes = useStyles();
    const [mobileView, setMobileView] = React.useState(false)
    const [experience , setExperience] = React.useState([
        {btnText: 'تطوير الأعمال', id: '1'},
        { btnText: 'المشاريع الصغيرة', id: '2' },
        { btnText: 'مشاركة العملاء', id: '3' }, 
        { btnText: 'البحث أمام الجمهور', id: '4' },
        { btnText: 'الدعاية والإعلان', id: '5' },
        { btnText: 'ريادة الأعمال', id: '6' },
        { btnText: 'التسويق الرقمي', id: '7' }, 
        { btnText: 'الإعلام', id: '8' },
        { btnText: 'القيادة', id: '9' }
    ])

    const MobilePlayer = () => {
        if (props.isMobile === true) {
            return (
                <GridItem xs={12} sm={12} md={7} >
                    <CustomMobilePlayer url='https://www.youtube.com/watch?v=I4wp0aHDQ5Q&feature=youtu.be' isMobile={props.isMobile} isWeb={props.isWeb} />
                </GridItem>

            )
        }
    }

    const WebPlayer = () => {
        if (props.isMobile === false) {
            return (

                <GridItem xs={12} sm={12} md={7} >
                    <CustomPlayer url='https://www.youtube.com/watch?v=I4wp0aHDQ5Q&feature=youtu.be' isWeb={props.isWeb} isMobile={props.isMobile} />
                </GridItem>

            )
        }
    }

    return(
        
        <GridContainer justify="center" >

            {MobilePlayer()}

            <GridItem xs={12} sm={12} md={4} className={classes.container}>

             <div className={classes.quoteContainer}>
             <Quote
                text="هند الناهض مستشارة إعلام رقمي, مستشارة الإعلام الإلكتروني لمكتب وكيل وزارة الإعلام الكويتية وهي المؤسسة لشركة “سوشالوبي” للخدمات المختصة في مجال الإعلام الإجتماعي , بالإضافة الى إنها مستشارة في مجال التواصل الاجتماعي.هند الناهض مستشارة إعلام رقمي, مستشارة الإعلام الإلكتروني لمكتب وكيل وزارة الإعلام الكويتية وهي المؤسسة لشركة “سوشالوبي” للخدمات المختصة في مجال الإعلام الإجتماعي , بالإضافة الى إنها مستشارة في مجال التواصل الاجتماعي."
                author="د.هند الناهض"
            />
            </div>

                
            <div dir='rtl'>
                 <h4 className={classes.titleTag} dir="rtl">الخبرات</h4>
                  {experience.map((exp) => (

                      <Button color="primary" size="cmd" >
                          {exp.btnText}
                      </Button>
                  ))}
            </div>

            </GridItem>
            
           
                {WebPlayer()}
        

            </GridContainer>
    )
}