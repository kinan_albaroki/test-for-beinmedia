import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import styles from "assets/jss/material-kit-react/components/typographyStyle.js";

const useStyles = makeStyles(styles);

export default function TestVideo() {
    const classes = useStyles();



    return (
        <div className={classes.testIconContainer}>
            <i className={'fas fa-video'} style={{ color: '#ffffff', fontSize: '10vw' }}></i>
        </div>
    )
}